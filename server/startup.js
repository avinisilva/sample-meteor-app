Meteor.startup(function() {
  Meteor.publish("tasks", function() {
    return Task.find({user: this.userId});
  });
});

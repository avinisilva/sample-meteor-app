Meteor.methods({
  addTask: function(item) {
    Task.insert({name: item.name, date: item.date, user: item.user});
  },

  removeTask: function(id) {
    Task.remove({_id: id, user: this.userId});
  }
});

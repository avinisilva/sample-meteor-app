Template.list.helpers({
  tasks: () => {
    return Task.find();
  },

  dateFormat: () => {
    return moment(this.date).format("DD/MM/YYYY HH:mm");
  }
});

Template.list.events({
  "click button": function (event, template) {
    var task = this;

    Meteor.call("removeTask", task._id);
  }
});

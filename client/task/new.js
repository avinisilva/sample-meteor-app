Template.new.events({
  "submit form": function(event, template) {
    event.preventDefault();

    let input = document.querySelector("form input[name='task']");

    Meteor.call("addTask", {name: input.value, date: new Date(), user: Meteor.userId()});

    input.value = "";
  }
});
